=========================================================================
Implementation Details
=========================================================================

Description
===========

Open-Closed design principle is used for implementing this requirement.
In other words, Strategy Design pattern has been used to implement the requirement.

With this approach, Developer will be free to implement any clock implementation by using generic Clock<T> INTERFACE.

For this requirement, I have implemented 2 implementation of Berlin Clock.
1. Using Java Calendar Class : BerlinCalendarClock.java
2. Using Java String Class : BerlinStringClock.java

Correct implementation which matches completely with all requirements is "BerlinStringClock.java".
Because below testcase would fail in "BerlinCalendarClock.java" implementation :
- 24:00:00 Y RRRR RRRR OOOOOOOOOOO OOOO

As 24:00:00 is same as 00:00:00 therefore BerlinCalendarClock.java will result same in both test cases.
However, BerlinStringClock.java class will result in different results and matches perfectly with requirements.

Design Details
==============
Context Class : TimeUtils.java
Algorithm Interface : Clock.java

TimeUtils.java contains the logic to display time using any implementation of Clock<T>.java class.
Clock.java is free to use any input time format datatype and can implement any implementation to grab 3 major parts of time, which are Hours, Minutes and Seconds.

To support better validation handling, InvalidInputTimeException.java class has been introduced.
