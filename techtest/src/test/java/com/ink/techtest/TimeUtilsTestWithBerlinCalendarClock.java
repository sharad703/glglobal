package com.ink.techtest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

import com.ink.techtest.clock.impl.BerlinCalendarClock;
import com.ink.techtest.exception.InvalidInputTimeException;

public class TimeUtilsTestWithBerlinCalendarClock {
	
	@Test
	public void testConvertTime_usingBerlinCalendarClock_0hr0min0sec() throws InvalidInputTimeException {
		Calendar inputTime = Calendar.getInstance();
		inputTime.set(Calendar.HOUR_OF_DAY, 0);
		inputTime.set(Calendar.MINUTE, 0);
		inputTime.set(Calendar.SECOND, 0);
		String expectedTime = "Y OOOO OOOO OOOOOOOOOOO OOOO";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinCalendarClock()));
	}
	
	@Test
	public void testConvertTime_usingBerlinCalendarClock_13hr17min01sec() throws InvalidInputTimeException {
		Calendar inputTime = Calendar.getInstance();
		inputTime.set(Calendar.HOUR_OF_DAY, 13);
		inputTime.set(Calendar.MINUTE, 17);
		inputTime.set(Calendar.SECOND, 1);
		String expectedTime = "O RROO RRRO YYROOOOOOOO YYOO";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinCalendarClock()));
	}
	
	@Test
	public void testConvertTime_usingBerlinCalendarClock_23hr59min59sec() throws InvalidInputTimeException {
		Calendar inputTime = Calendar.getInstance();
		inputTime.set(Calendar.HOUR_OF_DAY, 23);
		inputTime.set(Calendar.MINUTE, 59);
		inputTime.set(Calendar.SECOND, 59);
		String expectedTime = "O RRRR RRRO YYRYYRYYRYY YYYY";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinCalendarClock()));
	}
	
	@Test
	public void testConvertTime_usingBerlinCalendarClock_24hr00min00sec() throws InvalidInputTimeException {
		Calendar inputTime = Calendar.getInstance();
		inputTime.set(Calendar.HOUR_OF_DAY, 24);
		inputTime.set(Calendar.MINUTE, 0);
		inputTime.set(Calendar.SECOND, 0);
		String expectedTime = "Y RRRR RRRR OOOOOOOOOOO OOOO";
		assertThat(expectedTime, not(equalTo(TimeUtils.convertTime(inputTime, new BerlinCalendarClock()))));
	}

}
