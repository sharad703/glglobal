package com.ink.techtest.clockImpl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

import com.ink.techtest.Clock;
import com.ink.techtest.clock.impl.BerlinCalendarClock;
import com.ink.techtest.exception.InvalidInputTimeException;

public class BerlinCalendarClockTest {

	private Clock<Calendar> clock = null;
	
	@Test
	public void testGetHours_with0hrs() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.HOUR_OF_DAY, 0);
		String hoursRepresentation = clock.getHours(time);
		String expectedResult = "OOOO OOOO";
		assertEquals(expectedResult, hoursRepresentation);
	}
	
	@Test
	public void testGetHours_with24hrs() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.HOUR_OF_DAY, 24);
		String hoursRepresentation = clock.getHours(time);
		String expectedResult = "RRRR RRRR";
		assertThat(expectedResult, not(equalTo(hoursRepresentation)));
	}
	
	@Test
	public void testGetHours_with13hrs() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.HOUR_OF_DAY, 13);
		String hoursRepresentation = clock.getHours(time);
		String expectedResult = "RROO RRRO";
		assertEquals(expectedResult, hoursRepresentation);
	}

	@Test
	public void testGetMinutest_with17mins() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.MINUTE, 17);
		String minuteRepresentation = clock.getMinutes(time);
		String expectedResult = "YYROOOOOOOO YYOO";
		assertEquals(expectedResult, minuteRepresentation);
	}
	
	@Test
	public void testGetMinutest_with59mins() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.MINUTE, 59);
		String minuteRepresentation = clock.getMinutes(time);
		String expectedResult = "YYRYYRYYRYY YYYY";
		assertEquals(expectedResult, minuteRepresentation);
	}

	@Test
	public void testGetSeconds_with0sec() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.SECOND, 0);
		String secondRepresentation = clock.getSeconds(time);
		String expectedResult = "Y";
		assertEquals(expectedResult, secondRepresentation);
	}
	
	@Test
	public void testGetSeconds_with1sec() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.SECOND, 1);
		String secondRepresentation = clock.getSeconds(time);
		String expectedResult = "O";
		assertEquals(expectedResult, secondRepresentation);
	}
	
	@Test
	public void testGetSeconds_with46sec() throws InvalidInputTimeException {
		clock = new BerlinCalendarClock();
		Calendar time = Calendar.getInstance();
		time.set(Calendar.SECOND, 59);
		String secondRepresentation = clock.getSeconds(time);
		String expectedResult = "O";
		assertEquals(expectedResult, secondRepresentation);
	}

}
