package com.ink.techtest.clockImpl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ink.techtest.Clock;
import com.ink.techtest.clock.impl.BerlinStringClock;
import com.ink.techtest.exception.InvalidInputTimeException;

public class BerlinStringClockTest {

	private Clock<String> clock = null;

	@Test
	public void testGetHours_with0hrs() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "00:00:00";
		String hoursRepresentation = clock.getHours(time);
		String expectedResult = "OOOO OOOO";
		assertEquals(expectedResult, hoursRepresentation);
	}

	@Test
	public void testGetHours_with24hrs() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "24:00:00";
		String hoursRepresentation = clock.getHours(time);
		String expectedResult = "RRRR RRRR";
		assertEquals(expectedResult, hoursRepresentation);
	}

	@Test
	public void testGetHours_with13hrs() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "13:17:23";
		String hoursRepresentation = clock.getHours(time);
		String expectedResult = "RROO RRRO";
		assertEquals(expectedResult, hoursRepresentation);
	}

	@Test
	public void testGetMinutest_with17mins() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "13:17:23";
		String minuteRepresentation = clock.getMinutes(time);
		String expectedResult = "YYROOOOOOOO YYOO";
		assertEquals(expectedResult, minuteRepresentation);
	}

	@Test
	public void testGetMinutest_with59mins() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "23:59:59";
		String minuteRepresentation = clock.getMinutes(time);
		String expectedResult = "YYRYYRYYRYY YYYY";
		assertEquals(expectedResult, minuteRepresentation);
	}

	@Test
	public void testGetSeconds_with0sec() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "23:59:00";
		String secondRepresentation = clock.getSeconds(time);
		String expectedResult = "Y";
		assertEquals(expectedResult, secondRepresentation);
	}

	@Test
	public void testGetSeconds_with1sec() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "23:59:01";
		String secondRepresentation = clock.getSeconds(time);
		String expectedResult = "O";
		assertEquals(expectedResult, secondRepresentation);
	}

	@Test
	public void testGetSeconds_with46sec() throws InvalidInputTimeException {
		clock = new BerlinStringClock();
		String time = "23:59:46";
		String secondRepresentation = clock.getSeconds(time);
		String expectedResult = "Y";
		assertEquals(expectedResult, secondRepresentation);
	}
}
