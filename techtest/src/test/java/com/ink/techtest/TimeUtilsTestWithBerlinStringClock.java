package com.ink.techtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ink.techtest.clock.impl.BerlinStringClock;
import com.ink.techtest.exception.InvalidInputTimeException;

public class TimeUtilsTestWithBerlinStringClock {
	@Test
	public void testConvertTime_usingBerlinStringClock_0hr0min0sec() throws InvalidInputTimeException {
		String inputTime = "00:00:00";
		String expectedTime = "Y OOOO OOOO OOOOOOOOOOO OOOO";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinStringClock()));
	}
	
	@Test
	public void testConvertTime_usingBerlinStringClock_13hr17min01sec() throws InvalidInputTimeException {
		String inputTime = "13:17:01";
		String expectedTime = "O RROO RRRO YYROOOOOOOO YYOO";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinStringClock()));
	}
	
	@Test
	public void testConvertTime_usingBerlinStringClock_23hr59min59sec() throws InvalidInputTimeException {
		String inputTime = "23:59:59";
		String expectedTime = "O RRRR RRRO YYRYYRYYRYY YYYY";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinStringClock()));
	}
	
	@Test
	public void testConvertTime_usingBerlinStringClock_24hr00min00sec() throws InvalidInputTimeException {
		String inputTime = "24:00:00";
		String expectedTime = "Y RRRR RRRR OOOOOOOOOOO OOOO";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinStringClock()));
	}
	
	@Test(expected=InvalidInputTimeException.class)
	public void testConvertTime_usingBerlinStringClock_withInvalidInputTime() throws InvalidInputTimeException {
		String inputTime = "AA:BB:CC";
		String expectedTime = "Y RRRR RRRR OOOOOOOOOOO OOOO";
		assertEquals(expectedTime, TimeUtils.convertTime(inputTime, new BerlinStringClock()));
	}
}
