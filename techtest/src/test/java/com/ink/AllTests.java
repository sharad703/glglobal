package com.ink;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.ink.techtest.TimeUtilsTestWithBerlinCalendarClock;
import com.ink.techtest.TimeUtilsTestWithBerlinStringClock;
import com.ink.techtest.clockImpl.BerlinStringClockTest;
import com.ink.techtest.clockImpl.BerlinCalendarClockTest;

@RunWith(Suite.class)
@SuiteClasses({TimeUtilsTestWithBerlinCalendarClock.class,
	TimeUtilsTestWithBerlinStringClock.class,
	BerlinCalendarClockTest.class,
	BerlinStringClockTest.class})
public class AllTests {

}
