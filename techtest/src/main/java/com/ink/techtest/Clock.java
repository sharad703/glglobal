package com.ink.techtest;

import com.ink.techtest.exception.InvalidInputTimeException;


public interface Clock<T> {
	String getHours(T time) throws InvalidInputTimeException;
	String getMinutes(T time) throws InvalidInputTimeException;
	String getSeconds(T time) throws InvalidInputTimeException;
}
