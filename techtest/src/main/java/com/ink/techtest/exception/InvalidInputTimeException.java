package com.ink.techtest.exception;

public class InvalidInputTimeException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidInputTimeException(String message){
		super(message);
	}
}
