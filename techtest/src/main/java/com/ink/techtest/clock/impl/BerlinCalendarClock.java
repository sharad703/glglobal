package com.ink.techtest.clock.impl;

import java.util.Calendar;

import com.ink.techtest.Clock;
import com.ink.techtest.TimeUtils;

public class BerlinCalendarClock implements Clock<Calendar> {
	
	private static final String YELLOW_LAMP = "Y";
	private static final String RED_LAMP = "R";
	private static final String OFF_LAMP = "O";

	@Override
	public String getHours(Calendar time) {
		int hour = time.get(Calendar.HOUR_OF_DAY);
		int firstRowCount = hour / 5;
		int secondRowCount = hour - (firstRowCount * 5);
		StringBuilder hoursString = new StringBuilder();
		hoursString.append(getHourRowData(firstRowCount)).append(TimeUtils.SEPARATOR).append(getHourRowData(secondRowCount));
		return hoursString.toString();
	}

	@Override
	public String getMinutes(Calendar time) {
		int minutes = time.get(Calendar.MINUTE);
		int firstRow = minutes / 5;
		int secondRow = minutes - firstRow * 5;
		StringBuilder minutesString = new StringBuilder();
		minutesString.append(getFirstRowMinuteData(firstRow)).append(TimeUtils.SEPARATOR).append( getSecondRowMinuteData(secondRow));
		return  minutesString.toString();
	}

	@Override
	public String getSeconds(Calendar time) {
		int seconds = time.get(Calendar.SECOND);
		if ((seconds % 2) == 0) {
			return YELLOW_LAMP;
		} else {
			return OFF_LAMP;
		}
	}

	private String getHourRowData(int onLampCount) {
		StringBuilder hourCount = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			if (i < onLampCount) {
				hourCount.append(RED_LAMP);
			} else {
				hourCount.append(OFF_LAMP);
			}
		}
		return hourCount.toString();
	}

	private String getFirstRowMinuteData(int count) {
		StringBuilder minuteCount = new StringBuilder();
		for (int i = 0; i < 11; i++) {
			if (i < count) {
				if (i == 2 || i == 5 || i == 8) {
					minuteCount.append(RED_LAMP);
				} else {
					minuteCount.append(YELLOW_LAMP);
				}

			} else {
				minuteCount.append(OFF_LAMP);
			}
		}
		return minuteCount.toString();
	}

	private String getSecondRowMinuteData(int count) {
		StringBuilder minuteCount = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			if (i < count) {
				minuteCount.append(YELLOW_LAMP);
			} else {
				minuteCount.append(OFF_LAMP);
			}
		}
		return minuteCount.toString();
	}
}
