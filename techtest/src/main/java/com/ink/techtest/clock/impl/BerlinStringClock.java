package com.ink.techtest.clock.impl;

import com.ink.techtest.Clock;
import com.ink.techtest.TimeUtils;
import com.ink.techtest.exception.InvalidInputTimeException;

public class BerlinStringClock implements Clock<String> {

	private static final String YELLOW_LAMP = "Y";
	private static final String RED_LAMP = "R";
	private static final String OFF_LAMP = "O";

	private static final String INPUT_TIME_FORMAT_REGEX = "[0-2][0-9]:[0-5][0-9]:[0-9][0-9]";
	private static final String EXP_MSG_INVALID_TIME_FORMAT = "Invalid Time format. Time should be in HH:MM:SS.";

	@Override
	public String getHours(String time) throws InvalidInputTimeException {
		if (time.matches(INPUT_TIME_FORMAT_REGEX)) {
			int hour = Integer.parseInt(time.substring(0, 2));
			int firstRowCount = hour / 5;
			int secondRowCount = hour - (firstRowCount * 5);
			StringBuilder hoursString = new StringBuilder();
			hoursString.append(getHourRowData(firstRowCount)).append(TimeUtils.SEPARATOR).append(getHourRowData(secondRowCount));
			return hoursString.toString();
		} else {
			throw new InvalidInputTimeException(EXP_MSG_INVALID_TIME_FORMAT);
		}

	}

	@Override
	public String getMinutes(String time) throws InvalidInputTimeException {
		if (time.matches(INPUT_TIME_FORMAT_REGEX)) {
			int minutes = Integer.parseInt(time.substring(3, 5));
			int firstRow = minutes / 5;
			int secondRow = minutes - firstRow * 5;
			StringBuilder minutesString = new StringBuilder();
			minutesString.append(getFirstRowMinuteData(firstRow)).append(TimeUtils.SEPARATOR).append(getSecondRowMinuteData(secondRow));
			return minutesString.toString();
		} else {
			throw new InvalidInputTimeException(EXP_MSG_INVALID_TIME_FORMAT);
		}

	}

	@Override
	public String getSeconds(String time) throws InvalidInputTimeException {
		if (time.matches(INPUT_TIME_FORMAT_REGEX)) {
			int seconds = Integer.parseInt(time.substring(6));
			if ((seconds % 2) == 0) {
				return YELLOW_LAMP;
			} else {
				return OFF_LAMP;
			}
		} else {
			throw new InvalidInputTimeException(EXP_MSG_INVALID_TIME_FORMAT);
		}
	}

	private String getHourRowData(int onLampCount) {
		StringBuilder hourCount = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			if (i < onLampCount) {
				hourCount.append(RED_LAMP);
			} else {
				hourCount.append(OFF_LAMP);
			}
		}
		return hourCount.toString();
	}

	private String getFirstRowMinuteData(int count) {
		StringBuilder minuteCount = new StringBuilder();
		for (int i = 0; i < 11; i++) {
			if (i < count) {
				if (i == 2 || i == 5 || i == 8) {
					minuteCount.append(RED_LAMP);
				} else {
					minuteCount.append(YELLOW_LAMP);
				}

			} else {
				minuteCount.append(OFF_LAMP);
			}
		}
		return minuteCount.toString();
	}

	private String getSecondRowMinuteData(int count) {
		StringBuilder minuteCount = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			if (i < count) {
				minuteCount.append(YELLOW_LAMP);
			} else {
				minuteCount.append(OFF_LAMP);
			}
		}
		return minuteCount.toString();
	}

}
