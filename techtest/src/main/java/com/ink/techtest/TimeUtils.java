package com.ink.techtest;

import com.ink.techtest.exception.InvalidInputTimeException;

public final class TimeUtils {
	
	public static final String SEPARATOR = " ";
	
	public static <T> String convertTime(T time, Clock<T> clock) throws InvalidInputTimeException{
		StringBuilder timeBuilder = new StringBuilder();
		timeBuilder.append(clock.getSeconds(time)).append(SEPARATOR);
		timeBuilder.append(clock.getHours(time)).append(SEPARATOR);
		timeBuilder.append(clock.getMinutes(time));
		return timeBuilder.toString();
	}
	
}
